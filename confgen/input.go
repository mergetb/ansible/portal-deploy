package main

type InputConfig struct {
	Proxies        []string          `yaml:"proxies"`
	Masters        []string          `yaml:"masters"`
	ServiceWorkers []string          `yaml:"service_workers"`
	XdcWorkers     []string          `yaml:"xdc_workers"`
	StorageServers []string          `yaml:"storage_servers"`
	OpsServers     []string          `yaml:"ops_servers"`
	AddressSpace   AddressSpace      `yaml:"address_space"`
	CephConfig     CephConfig        `yaml:"ceph"`
	ClusterIfx     map[string]string `yaml:"cluster_ifx"`
	MergeConfig    MergeConfig       `yaml:"merge"`
	AuthConfig     AuthConfig        `yaml:"auth"`
}

type AddressSpace struct {
	Node       string `yaml:"node"`
	Pod        string `yaml:"pod"`
	Service    string `yaml:"service"`
	ServiceInt string `yaml:"service_int"`
	ServiceExt string `yaml:"service_ext"`
}

type ExternalAddressSpace struct {
	API    string `yaml:"api"`
	XDC    string `yaml:"xdc"`
	Launch string `yaml:"launch"`
}

type CephConfig struct {
	Disks              map[string][]string `ysml:"disks"`
	DefaultPoolSize    int                 `yaml:"default_pool_size"`
	DefaultMinPoolSize int                 `yaml:"default_min_pool_size"`
	DefaultPGNum       int                 `yaml:"default_pg_num"`
	DefaultPGPNum      int                 `yaml:"default_pgp_num"`
	CrushConfig        CrushConfig         `yaml:"crush"`
	CephFSConfig       CephFSConfig        `yaml:"cephfs"`
}

type CrushConfig struct {
	Host bool `yaml:"host"`
}

type CephFSConfig struct {
	Name     string `yaml:"name"`
	DataPool string `yaml:"data_pool"`
	MetaPool string `yaml:"meta_pool"`
}

type MergeAddresses struct {
	ClusterDNS string `yaml:"cluster_dns"`
}

type MergeConfig struct {
	Certs       map[string]string   `yaml:"certs"`
	Services    map[string]*Service `yaml:"services"`
	Addresses   MergeAddresses      `yaml:"addresses"`
	Replicas    int                 `yaml:"replicas"`
	Registry    Registry            `yaml:"registry"`
	AuxRegistry Registry            `yaml:"auxregistry"`
}

type AuthConfig struct {
	Certs        map[string]string   `yaml:"certs"`
	Namespace    string              `yaml:"namespace"`
	AuthDBConfig map[string]string   `yaml:"dbconfig"`
	OAuthConfig  map[string]string   `yaml:"oauth"`
	UserConfig   map[string]string   `yaml:"user"`
	Services     map[string]*Service `yaml:"services"`
	Registry     Registry            `yaml:"registry"`
}

type Registry struct {
	Reg string `yaml:"reg"`
	Org string `yaml:"org"`
	Tag string `yaml:"tag"`
}

type Service struct {
	Domain    string `yaml:"domain"`
	Addresses string `yaml:"addresses"`
	Anchor    string `yaml:"anchor"`
}
