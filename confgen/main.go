package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/goccy/go-yaml"
	"github.com/spf13/cobra"
)

const (
	version = "0.1.0"
)

func main() {

	log.SetFlags(0)

	name := filepath.Base(os.Args[0])

	root := cobra.Command{
		Use:     fmt.Sprintf("%s sourcefile", name),
		Short:   "generate a full portal deployment config",
		Version: version,
		Args:    cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			confgen(args[0])
		},
	}

	root.Execute()
}

func confgen(sourcefile string) {

	buf, err := ioutil.ReadFile(sourcefile)
	if err != nil {
		log.Fatal(err)
	}

	var input InputConfig
	err = yaml.Unmarshal(buf, &input)
	if err != nil {
		log.Fatal(err)
	}

	cfg := Config{Input: input}

	err = cfg.Configure(input)
	if err != nil {
		log.Fatal(err)
	}

	buf, err = yaml.Marshal(cfg.Output)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("portal-deploy.yml", buf, 0644)
	if err != nil {
		log.Fatal(err)
	}

}
