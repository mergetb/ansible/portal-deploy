package main

import (
	"encoding/binary"
	"fmt"
	"log"
	"net"
)

type IPIter struct {
	begin   uint32
	end     uint32
	current uint32
	b       []byte
}

func CreateIPIter(cidr string) (IPIter, error) {

	i := IPIter{
		b: []byte{0, 0, 0, 0},
	}

	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return i, fmt.Errorf("failed to read node address space: %v", err)
	}

	i.begin = binary.BigEndian.Uint32(ip.To4())
	size, _ := ipnet.Mask.Size()
	i.end = i.begin + uint32(1<<(32-size))
	i.current = i.begin
	binary.BigEndian.PutUint32(i.b, i.current)

	return i, nil

}

func (i *IPIter) Next() *IPIter {

	i.current++
	if i.current >= i.end {
		log.Fatalf("IP out of range")
	}
	binary.BigEndian.PutUint32(i.b, i.current)

	return i

}

func (i *IPIter) Jump(idx int) *IPIter {

	i.current = i.begin + uint32(idx)
	if i.current >= i.end {
		log.Fatalf("IP out of range")
	}
	binary.BigEndian.PutUint32(i.b, i.current)

	return i

}

func (i *IPIter) NextSubnet(size int) *IPIter {

	i.current += 1 << (32 - size)
	if i.current >= i.end {
		log.Fatalf("IP out of range")
	}
	binary.BigEndian.PutUint32(i.b, i.current)

	return i

}

func (i *IPIter) String() string {

	return net.IP(i.b).String()

}
