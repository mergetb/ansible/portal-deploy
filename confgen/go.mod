module gitlab.com/mergetb/ansible/portal-deploy

go 1.14

require (
	github.com/goccy/go-yaml v1.4.3
	github.com/spf13/cobra v1.0.0
)
