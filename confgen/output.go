package main

type OutputConfig struct {
	InputConfig     `yaml:"in"`
	EtcdServers     string                    `yaml:"etcd_servers"`
	EtcdCluster     string                    `yaml:"etcd_cluster"`
	MasterAddresses string                    `yaml:"master_addresses"`
	WorkerAddresses string                    `yaml:"worker_addresses"`
	Proxy           map[string]*ProxyConfig   `yaml:"proxy"`
	Master          map[string]*MasterConfig  `yaml:"master"`
	Worker          map[string]*WorkerConfig  `yaml:"worker"`
	Storage         map[string]*StorageConfig `yaml:"storage"`
}

type ProxyConfig struct {
	IP     string   `yaml:"ip"`
	SvcIPs []string `yaml:"svc_ips"`
}

type MasterConfig struct {
	IP string `yaml:"ip"`
}

type WorkerConfig struct {
	IP         string   `yaml:"ip"`
	PodSubnet  string   `yaml:"pod_subnet"`
	ServiceIPs []string `yaml:"svc_ips"`
}

type StorageConfig struct {
	IP string `yaml:"ip"`
}
