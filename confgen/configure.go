package main

import (
	"fmt"
	"strings"
)

const (
	// number of external services
	numExternalServiceIPs = 7

	// number of internal services
	numInternalServiceIPs = 1

	// number of internal services
	numInternalServiceIPs = 1

	// number of times a given external service IP is replicated
	serviceIPReplication = 3
)

type Config struct {
	Input        InputConfig
	Output       OutputConfig
	NodeIP       IPIter
	PodIP        IPIter
	ServiceIP    IPIter
	ServiceIntIP IPIter
	ServiceExtIP IPIter
}

func (c *Config) Configure(input InputConfig) error {

	c.Input = input
	c.Output.InputConfig = input

	err := c.InitIPs()
	if err != nil {
		return err
	}

	err = c.ConfigureProxies()
	if err != nil {
		return err
	}

	err = c.ConfigureMasters()
	if err != nil {
		return err
	}

	err = c.ConfigureWorkers()
	if err != nil {
		return err
	}

	err = c.ConfigureStorageServers()
	if err != nil {
		return err
	}

	c.ConfigureEtcd()

	return nil

}

func (c *Config) InitIPs() (err error) {

	c.NodeIP, err = CreateIPIter(c.Input.AddressSpace.Node)
	if err != nil {
		return
	}

	c.PodIP, err = CreateIPIter(c.Input.AddressSpace.Pod)
	if err != nil {
		return
	}

	c.ServiceIP, err = CreateIPIter(c.Input.AddressSpace.Service)
	if err != nil {
		return
	}

	c.ServiceIntIP, err = CreateIPIter(c.Input.AddressSpace.ServiceInt)
	if err != nil {
		return
	}

	c.ServiceExtIP, err = CreateIPIter(c.Input.AddressSpace.ServiceExt)
	if err != nil {
		return
	}

	return

}

func (c *Config) ConfigureProxies() error {

	c.Output.Proxy = make(map[string]*ProxyConfig)

	for _, x := range c.Input.Proxies {
		ip := c.NodeIP.Next().String()

		svcIntIP := c.ServiceIntIP.Next().String()
		svcExtIP := c.ServiceExtIP.Next().String()

		c.Output.Proxy[x] = &ProxyConfig{
			IP:     ip,
			SvcIPs: []string{svcIntIP, svcExtIP},
		}
	}

	return nil

}

func (c *Config) ConfigureMasters() error {

	c.Output.Master = make(map[string]*MasterConfig)

	var ips []string

	for _, x := range c.Input.Masters {
		ip := c.NodeIP.Next().String()
		c.Output.Master[x] = &MasterConfig{
			IP: ip,
		}
		ips = append(ips, ip)
	}
	c.Output.MasterAddresses += strings.Join(ips, ",")

	return nil

}

func (c *Config) ConfigureWorkers() error {

	c.Output.Worker = make(map[string]*WorkerConfig)

	workers := append(c.Input.XdcWorkers, c.Input.ServiceWorkers...)

	var ips []string

	for _, x := range workers {

		ip := c.NodeIP.Next().String()
		podSubnet := c.PodIP.String()

		c.Output.Worker[x] = &WorkerConfig{
			IP:        ip,
			PodSubnet: fmt.Sprintf("%s/24", podSubnet),
		}
		ips = append(ips, ip)

		c.PodIP.NextSubnet(24)

	}
	c.Output.WorkerAddresses += strings.Join(ips, ",")

	// Add internal services.
	for i := 1; i <= numInternalServiceIPs; i++ {

		c.ServiceIntIP.Jump(i * 10)

		for _, w := range c.Input.ServiceWorkers {
			c.Output.Worker[w].ServiceIPs = append(
				c.Output.Worker[w].ServiceIPs,
				c.ServiceIntIP.String(),
			)
		}
	}

	// add service ips in a round-robin
	for i := 0; i < serviceIPReplication; i++ {

		w := workers[i%len(workers)]

		for j := 1; j <= numExternalServiceIPs; j++ {

			c.ServiceExtIP.Jump(j*10 + i)

			c.Output.Worker[w].ServiceIPs = append(
				c.Output.Worker[w].ServiceIPs,
				c.ServiceExtIP.String(),
			)

		}

	}

	return nil

}

func (c *Config) ConfigureStorageServers() error {

	c.Output.Storage = make(map[string]*StorageConfig)

	for _, x := range c.Input.StorageServers {

		ip := c.NodeIP.Next().String()
		c.Output.Storage[x] = &StorageConfig{
			IP: ip,
		}

	}

	return nil

}

func (c *Config) ConfigureEtcd() {

	for name, x := range c.Output.Master {
		if c.Output.EtcdServers != "" {
			c.Output.EtcdServers += ","
		}
		if c.Output.EtcdCluster != "" {
			c.Output.EtcdCluster += ","
		}
		c.Output.EtcdServers += fmt.Sprintf("https://%s:2379", x.IP)
		c.Output.EtcdCluster += fmt.Sprintf("%s=https://%s:2380", name, x.IP)
	}

}
