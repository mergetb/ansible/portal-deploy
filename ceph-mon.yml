- name: initialize ceph.conf
  template:
    src: ceph.conf
    dest: /etc/ceph/ceph.conf
    mode: "0644"

- name: create mon keyring
  shell: >
    ceph-authtool 
    --create-keyring /tmp/ceph.mon.keyring 
    --gen-key -n mon. 
    --cap mon 'allow *'
  args:
    creates: /tmp/ceph.mon.keyring

- name: create admin keyring
  shell: >
    ceph-authtool 
    --create-keyring /etc/ceph/ceph.client.admin.keyring 
    --gen-key -n client.admin 
    --cap mon 'allow *' 
    --cap osd 'allow *' 
    --cap mds 'allow *' 
    --cap mgr 'allow *'
  args:
    creates: /etc/ceph/ceph.client.admin.keyring

- name: set appropriate permissions for admin keyring
  file:
    path: /etc/ceph/ceph.client.admin.keyring
    group: ceph
    mode: "0640"

- name: create a bootstrap osd keyring
  shell: >
    ceph-authtool 
    --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring 
    --gen-key -n client.bootstrap-osd 
    --cap mon 'profile bootstrap-osd'
  args:
    creates: /var/lib/ceph/bootstrap-osd/ceph.keyring

- name: add admin keys to mon keyring
  shell: >
    ceph-authtool /tmp/ceph.mon.keyring 
    --import-keyring /etc/ceph/ceph.client.admin.keyring

- name: add bootstrap osd keys to mon keyring
  shell: >
    ceph-authtool /tmp/ceph.mon.keyring
    --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring

- name: copy boostrap osd keys to etc (some tools expect this)
  copy:
    src: /var/lib/ceph/bootstrap-osd/ceph.keyring
    dest: /etc/ceph/ceph.client.bootstrap-osd.keyring
    mode: "0644"
    remote_src: true

- name: generate monmap
  shell: >
    monmaptool --create 
    --add {{ inventory_hostname }} 
    {{ storage[inventory_hostname].ip }} 
    --fsid {{ 'mergefs' | to_uuid }} /tmp/monmap
  args:
    creates: /tmp/monmap

- name: create mon data dir
  file:
    path: /var/lib/ceph/mon/ceph-{{ inventory_hostname }}
    state: directory

- name: populate monitor daemons with monmap and keyring
  shell: >
    ceph-mon 
    --mkfs -i {{ inventory_hostname }}
    --monmap /tmp/monmap 
    --keyring /tmp/ceph.mon.keyring

- name: ensure correct permissions on ceph runtime dir
  file:
    path: /var/lib/ceph
    owner: ceph
    group: ceph
    recurse: yes

- name: start ceph mon
  systemd:
    name: ceph-mon
    state: restarted
    enabled: true

# may take a few sconds for ceph mon to come up, so give this a retry loop
- name: enable messanger v2
  shell: ceph mon enable-msgr2
  retries: 10
  delay: 3
  register: result
  until: result.rc == 0

- name: create local directory for ceph configs
  file:
    path: /tmp/ceph-configs/ceph
    state: directory

- name: get ceph config files to local machine
  fetch:
    src: /etc/ceph/{{ item }}
    dest: /tmp/ceph-configs/ceph/{{ item }}
    flat: yes
  loop:
    - ceph.client.admin.keyring
    - ceph.conf
